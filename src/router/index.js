import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/Home'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '@/views/About')
    },
    {
      path: '/practice',
      name: 'practice',
      // 寫作方式 : 後面控一格
      component: () => import(/* webpackChunkName: "practice" */ '@/views/Practice')
    },
    {
      path: '/counter',
      name: 'counter',
      component: () => import(/* webpackChunkName: "counter" */ '@/views/Counter')
    },
    {
      path: '/search',
      name: 'search',
      component: () => import(/* webpackChunkName: "search" */ '@/views/Search')
    },
    {
      path: '/list',
      name: 'list',
      component: () => import(/* webpackChunkName: "search" */ '@/views/List' /* 記得資料夾名字要大寫 */)
    }
  ]
})
